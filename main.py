import Hse730Spider
import traceback

if __name__ == "__main__":
    try:
        Hse730Spider.main_flow()
        print()
        print("Works All Done.")
    except:
        with open("error_log.txt", 'w') as f:
            traceback.print_exc(file=f)
        print()
        print("Somethings go wrong! Please check file:")
        print("error_log.txt")
    print()
    input("Press any key to exit app >")