from Hse730Spider import general_tools


def extract(lines: list) -> dict:

    result = {}

    for i in range(len(lines)):

        line = lines[i]

        # this line, we extract advType, region and district
        if len(line) > 4 and line[:4] == "[主頁]":
            info = selector(line)
            result["advType"] = info[1]
            result["region"] = info[2]
            result["district"] = info[3]

        # we identify the image url and record it
        # duplicated urls are found, so we store them in dictionary
        elif len(line) >= 8 and line[:8] == "  * ![](" and "img.house730.com" in line:
            if not "img" in result:
                result["img"] = {}
            url = line.split("  * ![](")[1]
            url = url.split(")")[0]
            result["img"][url] = 0

        # rent price or buy price
        elif len(line) >= 3 and line[:2] == "**" and "__" in line:
            if line[:3] == "**售" or line[:3] == "**租":
                price = line.split("__")[1]
                price = price.replace("*", "")
                result["price"] = price

        # we got the number of rooms on cover page
        # in this page we only get the building age
        elif len(line) >= 2 and line[:2] == "間隔":
            x = line.split("**")
            for y in x:
                if "年" in y:
                    result["buildingAge"] = y.replace(" ", "")
                    break

        # address
        elif len(line) >= 4 and line[:4] == "樓盤地址":
            address = line.replace("樓盤地址", "")
            address = address.replace(" ", "")
            address = address.replace("\\\\--", "")
            result["address"] = address

        # phase
        elif len(line) >= 2 and line[:2] == "期數":
            phase = line.replace("期數", "")
            phase = phase.replace(" ", "")
            phase = phase.replace("\\", "")
            phase = phase.replace("-", "")
            result["phase"] = phase

        # unit
        elif len(line) >= 5 and line[:5] == "座位及單位":
            unit = line.replace("座位及單位", "")
            unit = unit.replace(" ", "")
            unit = unit.replace("\\", "")
            unit = unit.replace("-", "")
            result["unit"] = unit

        # management fee
        elif len(line) >= 3 and line[:3] == "管理費":
            managementFee = line.replace("管理費", "")
            managementFee = managementFee.replace(" ", "")
            managementFee = managementFee.replace("\\", "")
            managementFee = managementFee.replace("-", "")
            result["managementFee"] = managementFee

        # description
        elif len(line) >= 11 and line[:11] == "###### 單位特色":
            i2 = i + 1
            x = ""
            while not "物業編號" in lines[i2]:
                line = general_tools.process_text.removeAbnormalSpaces(
                    lines[i2])
                if not line == "":
                    x += line + "\n"
                i2 += 1
            result["description"] = x

        # date
        elif len(line) >= 6 and line[:6] == "刊登/續期日":
            result["date"] = line.replace("刊登/續期日", "")

        # contact
        elif len(line) >= 5 and line[:5] == "業主自讓盤" and "[ ![]() ](javascript" in lines[i+2]:
            i2 = i + 3
            while not "溫馨提示" in lines[i2]:
                if not lines[i2] == "" and not lines[i2][0] == " " and not "contactPerson" in result:
                    result["contactPerson"] = lines[i2]
                if len(lines[i2]) >= 2 and lines[i2][:2] == "__" and not "contactPhone" in result:
                    result["contactPhone"] = lines[i2].replace(
                        "__", "").replace(" ", "")
                i2 += 1

    return result


def selector(line: str) -> list:
    """
    select all strings between []
    """
    stringlist = []
    for i in range(len(line)):
        if line[i] == "[":
            x = ""
            i2 = i + 1
            while i2 < len(line) and not line[i2] == "]":
                x += line[i2]
                i2 += 1
            stringlist.append(x)
    return stringlist
