
def extract(lines: list) -> dict:

    results = {}

    for i in range(len(lines)):

        line = lines[i]

        # here, we use two conditions to confirm that
        # we have found the property title
        if len(line) > 0 and line[0] == "[" and not "![]" in line:
            if "(/buy-property-" in line or "(/rent-property-" in line:

                result = {}

                # # first line # #

                # adv title
                x = line.split("]")
                y = ""
                for i2 in range(len(x)-1):
                    y += x[i2]
                result["title"] = y[1:]

                # url
                x = line.split("](")[1]
                result["url"] = x[:-1]

                # id
                id = result["url"].replace("/rent-property-", "")
                id = id.replace("/buy-property-", "")
                id = id.replace(".html", "")

                # # line + 2 # #

                line = lines[i+2]
                x = line.split("__")

                # place
                y = x[1].split("](")[0]
                y = y.replace("[", "")
                y = y.replace("]", "")
                y = y.replace(" ", "")
                result["place"] = y

                # estate
                y = x[2].split("](")[0]
                y = y.replace("[", "")
                y = y.replace("]", "")
                y = y.replace(" ", "")
                result["estate"] = y

                # # line + 4 # #

                line = lines[i+4]
                x = line.split("  ")

                # net area
                for y in x:
                    if "呎" in y:
                        y = y.replace("呎", "")
                        y = y.replace(" ", "")
                        result["netArea"] = y
                        break

                # price per net area
                for y in x:
                    if "$" in y:
                        y = y.replace("@", "")
                        y = y.replace(" ", "")
                        result["pricePerNetArea"] = y
                        break

                # # line + 6 # #

                line = lines[i+6]
                x = line.split(" ")

                # build area
                for y in x:
                    if "呎" in y:
                        y = y.replace("呎", "")
                        y = y.replace(" ", "")
                        result["buildArea"] = y
                        break

                # price per build area
                for y in x:
                    if "$" in y:
                        y = y.replace("@", "")
                        y = y.replace(" ", "")
                        result["pricePerBuildArea"] = y
                        break

                # # line + 6 or line + 8 # #

                # sometimes, the building area is missing
                # we check line + 6 rather than line + 8
                if not "buildArea" in result:
                    line = lines[i+6]
                else:
                    line = lines[i+8]
                x = line.split("|")

                # buildingType
                result["buildingType"] = x[0].replace(" ", "")

                # room
                try:
                    result["room"] = x[1].replace(" ", "")
                except:
                    pass

                # zone
                try:
                    result["zone"] = x[2].replace(" ", "")
                except:
                    pass

                # # Record # #
                results[id] = result

    return results
