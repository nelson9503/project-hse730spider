import requests
import html2text


def get_web_text(url: str):
    """
    Get web text contents from url.
    """
    r = requests.get(url)
    lines = html2text.html2text(r.text).split("\n")
    return lines


def get_web_photo(url: str, savepath: str):
    """
    Download photo from url and save to file path.
    """
    try:
        r = requests.get(url)
        with open(savepath, 'wb') as f:
            f.write(r.content)
    except:
        pass
