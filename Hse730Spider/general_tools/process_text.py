
def select_lines(lines: list, start: str, end: str) -> list:
    """
    Select a part of lines between the start words and the end words.
    """
    newlines = []
    lock = True
    for line in lines:
        if len(line) >= len(start) and line[:len(start)] == start:
            lock = False
        if lock == True:
            continue
        if len(line) >= len(end) and line[:len(end)] == end:
            break
        line = removeAbnormalSpaces(line)
        newlines.append(line)
    return newlines


def removeAbnormalSpaces(x: str) -> str:
    """
    1. remove duoble spaces -> "  "
    2. remove spaces at start position
    3. remove spaces at ending position
    """
    while "  " in x:
        x = x.replace("  ", " ")
    while len(x) > 0 and x[0] == " ":
        x = x[1:]
    while len(x) > 0 and x[-1] == " ":
        x = x[:-1]
    return x
