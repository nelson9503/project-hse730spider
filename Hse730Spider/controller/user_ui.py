import os


class UI:

    def __init__(self):
        self.buyPhase1 = "waiting..."
        self.buyPhase2 = "waiting..."
        self.buyPhase3 = "waiting..."
        self.rentPhase1 = "waiting..."
        self.rentPhase2 = "waiting..."
        self.rentPhase3 = "waiting..."

    def report(self):
        os.system("cls")
        print()
        print("$$ HOUSE730 Spider $$")
        print("=====================")
        print()
        print("Buy Properties:")
        print("phase 1: {}".format(self.buyPhase1))
        print("phase 2: {}".format(self.buyPhase2))
        print("phase 3: {}".format(self.buyPhase3))
        print()
        print("Rent Properties:")
        print("phase 1: {}".format(self.rentPhase1))
        print("phase 2: {}".format(self.rentPhase2))
        print("phase 3: {}".format(self.rentPhase3))
        print("=====================")
