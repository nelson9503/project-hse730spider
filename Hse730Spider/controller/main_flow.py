import os
import datetime
import xlsxwriter as xw
from Hse730Spider import (
    cover_extractor,
    content_extractor
)
from Hse730Spider.controller.user_ui import UI
from Hse730Spider import general_tools


def main_flow():

    # web host
    webhost = "https://www.house730.com"

    # output file
    date = datetime.datetime.today()
    folder = "./hse730-{:04d}{:02d}{:02d}".format(
        date.year, date.month, date.day)
    if not os.path.exists(folder):
        os.mkdir(folder)
    if not os.path.exists(folder+"/image"):
        os.mkdir(folder+"/image")

    # create ui
    ui = UI()
    ui.report()

    # Buy Properties

    # scan pages
    results = {}
    g = 1
    while True:
        ui.buyPhase1 = "collecting properties information... | {}".format(g)
        ui.report()
        url = webhost + "/buy/t1s1g{}/".format(g)
        lines = general_tools.get_web_text(url)
        j = cover_extractor.extract(lines)
        if len(j) == 0:
            break
        for key in j:
            results[key] = j[key]
        g += 1
    ui.buyPhase1 = "Done."
    ui.report()

    # scan contents
    n = 0
    for id in results:
        n += 1
        ui.buyPhase2 = "collecting information per property... | {}/{}".format(
            n, len(results))
        ui.report()
        property = results[id]
        url = property["url"]
        lines = general_tools.get_web_text(webhost+url)
        j = content_extractor.extract(lines)
        for key in j:
            property[key] = j[key]
        results[id] = property
    ui.buyPhase2 = "Done."
    ui.report()

    # download images
    n = 0
    for id in results:
        n += 1
        ui.buyPhase3 = "downloading images per property... | {}/{}".format(
            n, len(results))
        ui.report()
        if not "img" in results[id]:
            continue
        image_urls = results[id]["img"]
        c = 0
        for image_url in image_urls:
            c += 1
            general_tools.get_web_photo(image_url, folder+"/image/"+str(id)+"-"+str(c)+".jpg")
    ui.buyPhase3 = "Done."
    ui.report()

    # file output
    wb = xw.Workbook(folder+"/buy_properties.xlsx")
    sh = wb.add_worksheet("output")
    headers = {
        "id":"A1",
        "title":"B1",
        "region":"C1",
        "district":"D1",
        "estate":"E1",
        "phase":"F1",
        "buildingType":"G1",
        "room":"H1",
        "floorZone":"I1",
        "unit":"J1",
        "buildingAge":"K1",
        "address":"L1",
        "netArea":"M1",
        "pricePerNetArea":"N1",
        "buildArea":"O1",
        "pricePerBuildArea":"P1",
        "price":"Q1",
        "date":"R1",
        "description":"S1",
        "contactPerson":"T1",
        "contactPhone":"U1",
        "link":"V1"
    }
    for header in headers:
        sh.write(headers[header], header)
    row = 2
    for id in results:
        info = results[id]
        sh.write("A"+str(row), id)
        sh.write("B"+str(row), info["title"])
        try:
            sh.write("C"+str(row), info["region"])
        except KeyError:
            pass
        try:
            sh.write("D"+str(row), info["district"])
        except KeyError:
            pass
        try:
            sh.write("E"+str(row), info["estate"])
        except KeyError:
            pass
        try:
            sh.write("F"+str(row), info["phase"])
        except KeyError:
            pass
        try:
            sh.write("G"+str(row), info["buildingType"])
        except KeyError:
            pass
        try:
            sh.write("H"+str(row), info["room"])
        except KeyError:
            pass
        try:
            sh.write("I"+str(row), info["zone"])
        except KeyError:
            pass
        try:
            sh.write("J"+str(row), info["unit"])
        except KeyError:
            pass
        try:
            sh.write("K"+str(row), info["buildingAge"])
        except KeyError:
            pass
        try:
            sh.write("L"+str(row), info["address"])
        except KeyError:
            pass
        try:
            sh.write("M"+str(row), info["netArea"])
        except KeyError:
            pass
        try:
            sh.write("N"+str(row), info["pricePerNetArea"])
        except KeyError:
            pass
        try:
            sh.write("O"+str(row), info["buildArea"])
        except KeyError:
            pass
        try:
            sh.write("P"+str(row), info["pricePerBuildArea"])
        except KeyError:
            pass
        try:
            sh.write("Q"+str(row), info["price"])
        except KeyError:
            pass
        try:
            sh.write("R"+str(row), info["date"])
        except KeyError:
            pass
        try:
            sh.write("S"+str(row), info["description"])
        except KeyError:
            pass
        try:
            sh.write("T"+str(row), info["contactPerson"])
        except KeyError:
            pass
        try:
            sh.write("U"+str(row), info["contactPhone"])
        except KeyError:
            pass
        try:
            sh.write_url("V"+str(row), webhost+info["url"])
        except KeyError:
            pass
        row += 1
    wb.close()

    # Rent Property

    # scan pages
    results = {}
    g = 1
    while True:
        ui.rentPhase1 = "collecting properties information... | {}".format(g)
        ui.report()
        url = webhost + "/rent/t1s1g{}/".format(g)
        lines = general_tools.get_web_text(url)
        j = cover_extractor.extract(lines)
        if len(j) == 0:
            break
        for key in j:
            results[key] = j[key]
        g += 1
    ui.rentPhase1 = "Done."
    ui.report()

    # scan contents
    n = 0
    for id in results:
        n += 1
        ui.rentPhase2 = "collecting information per property... | {}/{}".format(
            n, len(results))
        ui.report()
        property = results[id]
        url = property["url"]
        lines = general_tools.get_web_text(webhost+url)
        j = content_extractor.extract(lines)
        for key in j:
            property[key] = j[key]
        results[id] = property
    ui.rentPhase2 = "Done."
    ui.report()

    # download images
    n = 0
    for id in results:
        n += 1
        ui.rentPhase3 = "downloading images per property... | {}/{}".format(
            n, len(results))
        ui.report()
        if not "img" in results[id]:
            continue
        image_urls = results[id]["img"]
        c = 0
        for image_url in image_urls:
            c += 1
            general_tools.get_web_photo(image_url, folder+"/image/"+str(id)+"-"+str(c)+".jpg")
    ui.rentPhase3 = "Done."
    ui.report()

     # file output
    wb = xw.Workbook(folder+"/rent_properties.xlsx")
    sh = wb.add_worksheet("output")
    headers = {
        "id":"A1",
        "title":"B1",
        "region":"C1",
        "district":"D1",
        "estate":"E1",
        "phase":"F1",
        "buildingType":"G1",
        "room":"H1",
        "floorZone":"I1",
        "unit":"J1",
        "buildingAge":"K1",
        "address":"L1",
        "netArea":"M1",
        "pricePerNetArea":"N1",
        "buildArea":"O1",
        "pricePerBuildArea":"P1",
        "price":"Q1",
        "date":"R1",
        "description":"S1",
        "contactPerson":"T1",
        "contactPhone":"U1",
        "link":"V1"
    }
    for header in headers:
        sh.write(headers[header], header)
    row = 2
    for id in results:
        info = results[id]
        sh.write("A"+str(row), id)
        sh.write("B"+str(row), info["title"])
        try:
            sh.write("C"+str(row), info["region"])
        except KeyError:
            pass
        try:
            sh.write("D"+str(row), info["district"])
        except KeyError:
            pass
        try:
            sh.write("E"+str(row), info["estate"])
        except KeyError:
            pass
        try:
            sh.write("F"+str(row), info["phase"])
        except KeyError:
            pass
        try:
            sh.write("G"+str(row), info["buildingType"])
        except KeyError:
            pass
        try:
            sh.write("H"+str(row), info["room"])
        except KeyError:
            pass
        try:
            sh.write("I"+str(row), info["zone"])
        except KeyError:
            pass
        try:
            sh.write("J"+str(row), info["unit"])
        except KeyError:
            pass
        try:
            sh.write("K"+str(row), info["buildingAge"])
        except KeyError:
            pass
        try:
            sh.write("L"+str(row), info["address"])
        except KeyError:
            pass
        try:
            sh.write("M"+str(row), info["netArea"])
        except KeyError:
            pass
        try:
            sh.write("N"+str(row), info["pricePerNetArea"])
        except KeyError:
            pass
        try:
            sh.write("O"+str(row), info["buildArea"])
        except KeyError:
            pass
        try:
            sh.write("P"+str(row), info["pricePerBuildArea"])
        except KeyError:
            pass
        try:
            sh.write("Q"+str(row), info["price"])
        except KeyError:
            pass
        try:
            sh.write("R"+str(row), info["date"])
        except KeyError:
            pass
        try:
            sh.write("S"+str(row), info["description"])
        except KeyError:
            pass
        try:
            sh.write("T"+str(row), info["contactPerson"])
        except KeyError:
            pass
        try:
            sh.write("U"+str(row), info["contactPhone"])
        except KeyError:
            pass
        try:
            sh.write_url("V"+str(row), webhost+info["url"])
        except KeyError:
            pass
        row += 1
    wb.close()